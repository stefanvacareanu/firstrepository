Rails.application.routes.draw do
  resources :employees
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resource :hotels
   get '/users', to: 'users#index'
   get '/users/funk', to: 'users#function'
   #get '/cars', to: 'cars#index'
   post '/users', to: 'users#login'
   post '/users', to: 'users#logout'
end
