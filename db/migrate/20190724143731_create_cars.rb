class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :marca
      t.string :model
      t.integer :an
      t.string :proprietar

      t.timestamps
    end
  end
end
